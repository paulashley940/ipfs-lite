package threads.server.utils;

import android.net.Uri;
import android.webkit.URLUtil;

import androidx.annotation.NonNull;

import java.net.URI;
import java.util.Objects;

import threads.lite.cid.Cid;
import threads.server.LogUtils;
import threads.server.core.Content;


public class CodecDecider {

    private static final String TAG = CodecDecider.class.getSimpleName();

    private String multihash = null;
    private Codec codex = Codec.UNKNOWN;

    private CodecDecider() {
    }

    public static CodecDecider evaluate(@NonNull String code) {
        CodecDecider codecDecider = new CodecDecider();

        try {
            Uri uri = Uri.parse(code);
            if (uri != null) {
                if (Objects.equals(uri.getScheme(), Content.IPFS)) {
                    String multihash = uri.getHost();
                    try {
                        codecDecider.setMultihash(Cid.decode(multihash).String());
                        codecDecider.setCodex(Codec.IPFS_URI);
                        return codecDecider;
                    } catch (Throwable ignore) {
                    }
                } else if (Objects.equals(uri.getScheme(), Content.IPNS)) {
                    String multihash = uri.getHost();
                    codecDecider.setMultihash(multihash);
                    codecDecider.setCodex(Codec.IPNS_URI);
                    return codecDecider;

                }
            }
        } catch (Throwable e) {
            // ignore exception
        }

        try {


            try {
                code = code.trim();
                if (code.startsWith("\"") && code.endsWith("\"")) {
                    code = code.substring(1, code.length() - 1);
                }

            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

            // check if code is Cid
            try {
                codecDecider.setMultihash(Cid.decode(code).String());
                codecDecider.setCodex(Codec.MULTIHASH);
                return codecDecider;
            } catch (Throwable throwable) {
                LogUtils.warning(TAG, throwable.getMessage());
            }


            if (URLUtil.isValidUrl(code)) {
                // ok now it is a URI, but is the content is an ipfs multihash

                URI uri = new URI(code);
                String path = uri.getPath();
                if (path.startsWith("/" + Content.IPFS + "/")) {
                    String multihash = path.replaceFirst("/" + Content.IPFS + "/", "");
                    multihash = trim(multihash);
                    try {
                        codecDecider.setMultihash(Cid.decode(multihash).String());
                        codecDecider.setCodex(Codec.IPFS_URI);
                        return codecDecider;
                    } catch (Throwable ignore) {
                    }
                } else if (path.startsWith("/" + Content.IPNS + "/")) {
                    String multihash = path.replaceFirst("/" + Content.IPNS + "/", "");
                    multihash = trim(multihash);
                    try {
                        codecDecider.setMultihash(Cid.decode(multihash).String());
                        codecDecider.setCodex(Codec.IPNS_URI);
                        return codecDecider;
                    } catch (Throwable ignore) {
                    }
                }

            }
        } catch (Throwable e) {
            // ignore exception
        }


        codecDecider.setCodex(Codec.UNKNOWN);
        return codecDecider;
    }


    private static String trim(@NonNull String data) {
        int index = data.indexOf("/");
        if (index > 0) {
            return data.substring(0, index);
        }
        return data;
    }

    public String getMultihash() {
        return multihash;
    }

    private void setMultihash(String multihash) {
        this.multihash = multihash;
    }

    public Codec getCodex() {
        return codex;
    }

    private void setCodex(Codec codex) {
        this.codex = codex;
    }


    public enum Codec {
        UNKNOWN, MULTIHASH, IPFS_URI, IPNS_URI
    }
}
