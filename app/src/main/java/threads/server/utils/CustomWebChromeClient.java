package threads.server.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.core.view.WindowCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.core.view.WindowInsetsControllerCompat;

import threads.server.MainActivity;

public class CustomWebChromeClient extends WebChromeClient {
    private final Activity mActivity;
    private View mCustomView;
    private WebChromeClient.CustomViewCallback mCustomViewCallback;

    public CustomWebChromeClient(@NonNull Activity activity) {
        this.mActivity = activity;
    }

    @Override
    public void onHideCustomView() {
        ((FrameLayout) mActivity.getWindow().getDecorView()).removeView(this.mCustomView);
        this.mCustomView = null;
        showSystemUI();
        this.mCustomViewCallback.onCustomViewHidden();
        this.mCustomViewCallback = null;
    }

    @Override
    public boolean onCreateWindow(WebView view, boolean dialog, boolean userGesture, android.os.Message resultMsg) {
        WebView.HitTestResult result = view.getHitTestResult();
        String data = result.getExtra();
        Context context = view.getContext();
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(data), context, MainActivity.class);
        context.startActivity(browserIntent);
        return false;
    }

    @Override
    public void onShowCustomView(View paramView, WebChromeClient.CustomViewCallback paramCustomViewCallback) {
        this.mCustomView = paramView;
        this.mCustomViewCallback = paramCustomViewCallback;
        ((FrameLayout) mActivity.getWindow().getDecorView())
                .addView(this.mCustomView, new FrameLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER);
        hideSystemUI();

    }

    private void hideSystemUI() {
        WindowCompat.setDecorFitsSystemWindows(mActivity.getWindow(), false);
        WindowInsetsControllerCompat controller = new WindowInsetsControllerCompat(
                mActivity.getWindow(), this.mCustomView);
        controller.hide(WindowInsetsCompat.Type.systemBars());
        controller.setSystemBarsBehavior(WindowInsetsControllerCompat.BEHAVIOR_SHOW_BARS_BY_TOUCH);
    }

    private void showSystemUI() {
        WindowCompat.setDecorFitsSystemWindows(mActivity.getWindow(), true);
        new WindowInsetsControllerCompat(mActivity.getWindow(), mActivity.getWindow().getDecorView())
                .show(WindowInsetsCompat.Type.systemBars());
    }

    @Override
    public Bitmap getDefaultVideoPoster() {
        return Bitmap.createBitmap(10, 10, Bitmap.Config.ARGB_8888);
    }

    @Override
    public void onProgressChanged(WebView view, int newProgress) {
        super.onProgressChanged(view, newProgress);
    }

}
