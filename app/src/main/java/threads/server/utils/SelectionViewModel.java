package threads.server.utils;

import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.Objects;

import threads.server.core.files.SortOrder;

public class SelectionViewModel extends ViewModel {

    @NonNull
    private final MutableLiveData<Long> parentProxyFile = new MutableLiveData<>(0L);
    @NonNull
    private final MutableLiveData<Boolean> showFab = new MutableLiveData<>(true);
    @NonNull
    private final MutableLiveData<String> query = new MutableLiveData<>("");
    @NonNull
    private final MutableLiveData<SortOrder> sortOrder = new MutableLiveData<>(SortOrder.DATE);

    @NonNull
    private final MutableLiveData<Uri> uri = new MutableLiveData<>(null);

    @NonNull
    public MutableLiveData<Long> getParentProxyFile() {
        return parentProxyFile;
    }

    public void setParentProxyFile(long idx) {
        getParentProxyFile().postValue(idx);
    }

    public long getParentProxyFileValue() {
        Long parentFile = getParentProxyFile().getValue();
        return Objects.requireNonNull(parentFile);
    }

    @NonNull
    public MutableLiveData<String> getQuery() {
        return query;
    }

    public void setQuery(@NonNull String query) {
        getQuery().postValue(query);
    }

    @NonNull
    public MutableLiveData<SortOrder> getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(@NonNull SortOrder sortOrder) {
        getSortOrder().postValue(sortOrder);
    }

    @NonNull
    public MutableLiveData<Uri> getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        getUri().postValue(uri);
    }

    @NonNull
    public MutableLiveData<Boolean> getShowFab() {
        return showFab;
    }

    public void setShowFab(boolean show) {
        getShowFab().postValue(show);
    }

}
