package threads.server.core.files;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Room;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import threads.lite.cid.Cid;

public class FILES {

    private static volatile FILES INSTANCE = null;

    private final FilesDatabase filesDatabase;


    private FILES(FilesDatabase filesDatabase) {
        this.filesDatabase = filesDatabase;
    }

    @NonNull
    private static FILES createFiles(@NonNull FilesDatabase filesDatabase) {
        return new FILES(filesDatabase);
    }

    public static FILES getInstance(@NonNull Context context) {

        if (INSTANCE == null) {
            synchronized (FILES.class) {
                if (INSTANCE == null) {
                    FilesDatabase filesDatabase = Room.databaseBuilder(context,
                                    FilesDatabase.class,
                                    FilesDatabase.class.getSimpleName()).
                            fallbackToDestructiveMigration().
                            build();
                    INSTANCE = FILES.createFiles(filesDatabase);
                }
            }
        }
        return INSTANCE;
    }


    public void clear() {
        getFilesDatabase().clearAllTables();
    }

    @NonNull
    public FilesDatabase getFilesDatabase() {
        return filesDatabase;
    }

    public void setFilesDeleting(long... idxs) {
        for (long idx : idxs) {
            getFilesDatabase().proxyDao().setDeleting(idx);
        }
    }

    public void resetFilesDeleting(long... idxs) {
        for (long idx : idxs) {
            getFilesDatabase().proxyDao().resetDeleting(idx);
        }
    }

    public void setFileLeaching(long idx) {
        getFilesDatabase().proxyDao().setLeaching(idx);
    }

    public void resetThreadLeaching(long idx) {
        getFilesDatabase().proxyDao().resetLeaching(idx);
    }

    public void setFileDone(long idx) {
        getFilesDatabase().proxyDao().setDone(idx);
    }

    public void setFileDone(long idx, @NonNull Cid cid) {
        getFilesDatabase().proxyDao().setDone(idx, cid);
    }

    public List<Proxy> getAncestors(long idx) {
        List<Proxy> path = new ArrayList<>();
        if (idx > 0) {
            Proxy proxy = getFileByIdx(idx);
            if (proxy != null) {
                path.addAll(getAncestors(proxy.getParent()));
                path.add(proxy);
            }
        }
        return path;
    }

    @NonNull
    public Proxy createFile(long parent) {
        return Proxy.createProxyFile(parent);
    }

    public boolean isReferenced(@NonNull Cid cid) {
        return getFilesDatabase().proxyDao().references(cid) > 0;
    }

    public void removeFile(@NonNull Proxy proxy) {
        getFilesDatabase().proxyDao().removeProxy(proxy);
    }

    public long storeFile(@NonNull Proxy proxy) {
        return getFilesDatabase().proxyDao().insertProxy(proxy);
    }

    public void setFileName(long idx, @NonNull String name) {
        getFilesDatabase().proxyDao().setName(idx, name);
    }

    public void setFileContent(long idx, @NonNull Cid cid) {
        getFilesDatabase().proxyDao().setContent(idx, cid);
    }

    public long getParentFile(long idx) {
        return getFilesDatabase().proxyDao().getParent(idx);
    }

    public String getFileName(long idx) {
        return getFilesDatabase().proxyDao().getName(idx);
    }

    @NonNull
    public List<Proxy> getPins() {
        return getFilesDatabase().proxyDao().getPins();
    }

    public long getChildrenSummarySize(long parent) {
        return getFilesDatabase().proxyDao().getChildrenSummarySize(parent);
    }

    @NonNull
    public List<Proxy> getChildren(long parent) {
        return getFilesDatabase().proxyDao().getChildren(parent);
    }

    @Nullable
    public Proxy getFileByIdx(long idx) {
        return getFilesDatabase().proxyDao().getProxyByIdx(idx);
    }

    @Nullable
    public Cid getFileContent(long idx) {
        return getFilesDatabase().proxyDao().getContent(idx);
    }

    @NonNull
    public List<Proxy> getFilesByNameAndParent(@NonNull String name, long parent) {

        return getFilesDatabase().proxyDao().getProxiesByNameAndParent(name, parent);
    }

    public void setFileSize(long idx, long size) {
        getFilesDatabase().proxyDao().setSize(idx, size);
    }

    public void setFileWork(long idx, @NonNull UUID id) {
        getFilesDatabase().proxyDao().setWork(idx, id.toString());
    }

    public void resetFileWork(long idx) {
        getFilesDatabase().proxyDao().resetWork(idx);
    }


    public List<Proxy> getDeletedFiles() {
        return getFilesDatabase().proxyDao().getDeletedProxies(System.currentTimeMillis());
    }

    public void setFileLastModified(long idx, long time) {
        getFilesDatabase().proxyDao().setLastModified(idx, time);
    }

    public void setFileUri(long idx, @NonNull String uri) {
        getFilesDatabase().proxyDao().setUri(idx, uri);
    }
}
