package threads.server.core.files;

import android.provider.DocumentsContract;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverter;
import androidx.room.TypeConverters;

import java.util.Objects;
import java.util.UUID;

import threads.lite.cid.Cid;

@androidx.room.Entity
public class Proxy {

    @ColumnInfo(name = "parent")
    private final long parent; // checked
    @PrimaryKey(autoGenerate = true)
    private long idx; // checked
    @ColumnInfo(name = "lastModified")
    private long lastModified; // checked
    @Nullable
    @ColumnInfo(name = "cid")
    @TypeConverters(Proxy.class)
    private Cid cid;
    @ColumnInfo(name = "size")
    private long size;  // checked
    @NonNull
    @ColumnInfo(name = "mimeType")
    private String mimeType;  // checked
    @NonNull
    @ColumnInfo(name = "name")
    private String name = "";
    @Nullable
    @ColumnInfo(name = "uri")
    private String uri;
    @Nullable
    @ColumnInfo(name = "work")
    private String work;
    @ColumnInfo(name = "leaching")
    private boolean leaching; // checked
    @ColumnInfo(name = "seeding")
    private boolean seeding; // checked
    @ColumnInfo(name = "deleting")
    private boolean deleting; // checked

    Proxy(long parent) {
        this.cid = null;
        this.parent = parent;
        this.lastModified = System.currentTimeMillis();
        this.mimeType = "";

        this.leaching = false;
        this.seeding = false;
        this.deleting = false;
    }


    @Nullable
    @TypeConverter
    public static Cid fromArray(byte[] data) {
        return Cid.fromArray(data);
    }

    @Nullable
    @TypeConverter
    public static byte[] toArray(Cid cid) {
        return Cid.toArray(cid);
    }

    static Proxy createProxyFile(long parent) {
        return new Proxy(parent);
    }

    @Nullable
    public Cid getCid() {
        return cid;
    }

    public void setCid(@Nullable Cid cid) {
        this.cid = cid;
    }

    @Nullable
    public String getUri() {
        return uri;
    }

    public void setUri(@Nullable String uri) {
        this.uri = uri;
    }

    public boolean isLeaching() {
        return leaching;
    }

    public void setLeaching(boolean leaching) {
        this.leaching = leaching;
    }


    public long getLastModified() {
        return lastModified;
    }

    public void setLastModified(long lastModified) {
        this.lastModified = lastModified;
    }


    public long getIdx() {
        return idx;
    }

    void setIdx(long idx) {
        this.idx = idx;
    }

    @NonNull
    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(@NonNull String mimeType) {
        this.mimeType = mimeType;
    }

    public boolean areItemsTheSame(@NonNull Proxy proxy) {
        return idx == proxy.getIdx();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Proxy proxy = (Proxy) o;
        return getIdx() == proxy.getIdx();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIdx());
    }

    public long getParent() {
        return parent;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    public boolean isDir() {
        return DocumentsContract.Document.MIME_TYPE_DIR.equals(getMimeType());
    }

    public boolean isSeeding() {
        return seeding;
    }

    public void setSeeding(boolean seeding) {
        this.seeding = seeding;
    }

    public boolean isDeleting() {
        return deleting;
    }

    public void setDeleting(boolean deleting) {
        this.deleting = deleting;
    }

    @Nullable
    public String getWork() {
        return work;
    }

    public void setWork(@Nullable String work) {
        this.work = work;
    }

    @Nullable
    public UUID getWorkUUID() {
        if (work != null) {
            return UUID.fromString(work);
        }
        return null;
    }

}
