package threads.server.services;

import android.content.Context;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;

import androidx.annotation.NonNull;

import java.net.Inet6Address;
import java.net.InetAddress;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.lite.IPFS;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.cid.Protocol;
import threads.lite.core.Connection;
import threads.lite.utils.TimeoutCancellable;
import threads.server.LogUtils;
import threads.server.core.DOCS;

public class DiscoveryService implements NsdManager.DiscoveryListener {
    private static final String TAG = DiscoveryService.class.getSimpleName();

    @NonNull
    private final IPFS ipfs;
    @NonNull
    private final DOCS docs;
    @NonNull
    private final NsdManager nsdManager;
    public DiscoveryService(@NonNull Context context, @NonNull NsdManager nsdManager) throws Exception {
        this.docs = DOCS.getInstance(context);
        this.ipfs = IPFS.getInstance(context);
        this.nsdManager = nsdManager;
    }

    @Override
    public void onStartDiscoveryFailed(String serviceType, int errorCode) {
        LogUtils.debug(TAG, "onStartDiscoveryFailed");
    }

    @Override
    public void onStopDiscoveryFailed(String serviceType, int errorCode) {
        LogUtils.debug(TAG, "onStopDiscoveryFailed");
    }

    @Override
    public void onDiscoveryStarted(String serviceType) {
        LogUtils.debug(TAG, "onDiscoveryStarted");
    }

    @Override
    public void onDiscoveryStopped(String serviceType) {
        LogUtils.debug(TAG, "onDiscoveryStopped");
    }


    @Override
    public void onServiceFound(NsdServiceInfo serviceInfo) {
        nsdManager.resolveService(serviceInfo, new NsdManager.ResolveListener() {

            @Override
            public void onResolveFailed(NsdServiceInfo nsdServiceInfo, int i) {
                LogUtils.error(TAG, "onResolveFailed " + nsdServiceInfo.toString());
            }

            @Override
            public void onServiceResolved(NsdServiceInfo nsdServiceInfo) {
                try {
                    LogUtils.error(TAG, "onResolveResolved " + nsdServiceInfo.toString());
                    boolean connect = !Objects.equals(ipfs.self().toBase58(),
                            nsdServiceInfo.getServiceName());
                    if (connect) {
                        connect(nsdServiceInfo);
                    }
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }
        });
    }

    @Override
    public void onServiceLost(NsdServiceInfo serviceInfo) {
        LogUtils.error(TAG, "onServiceLost " + serviceInfo.toString());
        try {

            try {
                PeerId peerId = PeerId.fromString(serviceInfo.getServiceName());
                docs.removeSwarm(peerId);
            } catch (Throwable ignore) {
                // not a regular node
                Map<String, byte[]> maps = serviceInfo.getAttributes();
                byte[] value = maps.get(Protocol.DNSADDR.getType());
                String address = new String(value);

                Multiaddr multiaddr = ipfs.decodeMultiaddr(address);
                PeerId peerId = multiaddr.getPeerId();
                docs.removeSwarm(peerId);
            }

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }


    public void connect(@NonNull NsdServiceInfo serviceInfo) {

        AtomicBoolean regularNode = new AtomicBoolean(false);
        try {
            PeerId.fromString(serviceInfo.getServiceName());
        } catch (Throwable throwable) {
            regularNode.set(true);
        }

        try {
            if (!regularNode.get()) {
                InetAddress inetAddress = serviceInfo.getHost();

                String pre = "/ip4";
                if (inetAddress instanceof Inet6Address) {
                    pre = "/ip6";
                }

                String multiAddress = pre + inetAddress + "/udp/" + serviceInfo.getPort()
                        + "/quic/p2p/" + serviceInfo.getServiceName();
                Multiaddr multiaddr = ipfs.decodeMultiaddr(multiAddress);
                PeerId peerId = multiaddr.getPeerId();
                if (docs.notWithinSwarm(peerId)) {
                    Executors.newSingleThreadExecutor().execute(() -> {
                        try {
                            Connection connection = ipfs.dial(docs.getSession(),
                                    multiaddr, ipfs.getConnectionParameters());
                            Objects.requireNonNull(connection);
                            docs.addSwarm(connection);
                        } catch (Throwable ignore) {
                        }
                    });
                }

            } else {
                Map<String, byte[]> maps = serviceInfo.getAttributes();
                byte[] value = maps.get(Protocol.DNSADDR.getType());
                String address = new String(value);

                Multiaddr multiaddr = ipfs.decodeMultiaddr(address);
                PeerId peerId = multiaddr.getPeerId();
                if (docs.notWithinSwarm(peerId)) {
                    Executors.newSingleThreadExecutor().execute(() -> {
                        try {
                            ipfs.findPeer(docs.getSession(), peerId, multiaddr1 -> {
                                if (!multiaddr1.isCircuitAddress()) {
                                    try {
                                        Connection connection = ipfs.dial(docs.getSession(),
                                                multiaddr1, ipfs.getConnectionParameters());
                                        Objects.requireNonNull(connection);

                                        docs.addSwarm(connection);
                                    } catch (Throwable ignore) {
                                        // default
                                    }
                                }
                            }, new TimeoutCancellable(30));
                        } catch (Throwable ignore) {
                        }
                    });
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }

}
