package threads.server.services;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.os.IBinder;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.work.ExistingWorkPolicy;

import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.lite.IPFS;
import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.cid.Protocol;
import threads.lite.core.IpnsRecord;
import threads.lite.core.Reachability;
import threads.server.InitApplication;
import threads.server.LogUtils;
import threads.server.MainActivity;
import threads.server.R;
import threads.server.Settings;
import threads.server.core.Content;
import threads.server.core.DOCS;
import threads.server.core.pages.Page;
import threads.server.work.ReservationWorker;

public class DaemonService extends Service {

    public static final AtomicBoolean STARTED = new AtomicBoolean(false);
    private static final String TAG = DaemonService.class.getSimpleName();
    public static Reachability REACHABILITY = Reachability.UNKNOWN;
    private ConnectivityManager.NetworkCallback networkCallback;
    private NsdManager nsdManager;
    private DiscoveryService discoveryService;

    public static void start(@NonNull Context context) {

        try {
            Intent intent = new Intent(context, DaemonService.class);
            intent.putExtra(Content.REFRESH, true);
            ContextCompat.startForegroundService(context, intent);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void unRegisterNetworkCallback() {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager)
                    getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

            connectivityManager.unregisterNetworkCallback(networkCallback);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void registerNetworkCallback() {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager)
                    getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

            networkCallback = new ConnectivityManager.NetworkCallback() {
                @Override
                public void onAvailable(Network network) {
                    try {
                        ReservationWorker.reservations(getApplicationContext(),
                                ExistingWorkPolicy.REPLACE, 0);
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                }

                @Override
                public void onLost(Network network) {
                    try {
                        IPFS.getInstance(getApplicationContext()).updateNetwork();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                }
            };


            connectivityManager.registerDefaultNetworkCallback(networkCallback);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void stop() {
        try {
            stopForeground(STOP_FOREGROUND_REMOVE);
        } finally {
            stopSelf();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        try {

            if (intent.getBooleanExtra(Content.REFRESH, false)) {
                buildNotification();
            } else {
                stop();
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

        return START_NOT_STICKY;
    }

    private void buildNotification() {
        try {
            IPFS ipfs = IPFS.getInstance(getApplicationContext());
            int port = ipfs.getPort();
            NotificationCompat.Builder builder = new NotificationCompat.Builder(
                    getApplicationContext(), InitApplication.DAEMON_CHANNEL_ID);

            Intent notifyIntent = new Intent(getApplicationContext(), MainActivity.class);
            int viewID = (int) System.currentTimeMillis();
            PendingIntent viewIntent = PendingIntent.getActivity(getApplicationContext(),
                    viewID, notifyIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);


            Intent stopIntent = new Intent(getApplicationContext(), DaemonService.class);
            stopIntent.putExtra(Content.REFRESH, false);
            int requestID = (int) System.currentTimeMillis();
            PendingIntent stopPendingIntent = PendingIntent.getService(
                    getApplicationContext(), requestID, stopIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

            String cancel = getApplicationContext().getString(android.R.string.cancel);
            NotificationCompat.Action action = new NotificationCompat.Action.Builder(
                    R.drawable.pause, cancel, stopPendingIntent).build();
            builder.setSmallIcon(R.drawable.access_point_network);
            int connections = ipfs.numServerConnections();
            if (connections > 0) {
                builder.setColor(Color.parseColor("#006972"));
            }
            builder.setSubText(getApplicationContext().getString(
                    R.string.server_connections) + " " + connections);
            builder.addAction(action);
            builder.setOnlyAlertOnce(true);

            String text = "";
            if (REACHABILITY == Reachability.UNKNOWN) {
                text = getString(R.string.service_reachable_unknown);
            } else if (REACHABILITY == Reachability.LOCAL) {
                text = getString(R.string.service_local_reachable);
            } else if (REACHABILITY == Reachability.NONE) {
                text = getString(R.string.service_not_reachable);
            } else if (REACHABILITY == Reachability.GLOBAL) {
                text = getString(R.string.service_reachable);
            }

            builder.setContentTitle(getString(R.string.service_is_running, String.valueOf(port)));
            builder.setStyle(new NotificationCompat.BigTextStyle().bigText(text));
            builder.setContentIntent(viewIntent);
            builder.setGroup(InitApplication.DAEMON_GROUP_ID);
            builder.setCategory(Notification.CATEGORY_SERVICE);


            Notification notification = builder.build();
            startForeground(TAG.hashCode(), notification);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void registerService() {
        try {
            IPFS ipfs = IPFS.getInstance(getApplicationContext());
            DOCS docs = DOCS.getInstance(getApplicationContext());
            PeerId peerId = ipfs.self();
            String ownServiceName = peerId.toBase58();


            NsdServiceInfo serviceInfo = new NsdServiceInfo();
            try {
                Multiaddr address = docs.getSiteLocalAddress();
                Objects.requireNonNull(address);
                serviceInfo.setAttribute(Protocol.DNSADDR.getType(), address.toString());
                LogUtils.error(TAG, Protocol.DNSADDR.getType() + "=" + address);
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
            serviceInfo.setServiceName(ownServiceName);
            serviceInfo.setServiceType(Settings.SERVICE);
            serviceInfo.setPort(ipfs.getPort());
            nsdManager = (NsdManager) getSystemService(Context.NSD_SERVICE);
            Objects.requireNonNull(nsdManager);
            nsdManager.registerService(serviceInfo, NsdManager.PROTOCOL_DNS_SD,
                    RegistrationService.getInstance());

            discoveryService = new DiscoveryService(getApplicationContext(), nsdManager);
            nsdManager.discoverServices(
                    Settings.SERVICE, NsdManager.PROTOCOL_DNS_SD, discoveryService);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void unRegisterService() {
        try {
            if (nsdManager != null) {
                nsdManager.unregisterService(RegistrationService.getInstance());
                if (discoveryService != null) {
                    nsdManager.stopServiceDiscovery(discoveryService);
                    discoveryService = null;
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            unRegisterNetworkCallback();
            unRegisterService();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            STARTED.set(false);
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        try {
            ExecutorService service = Executors.newSingleThreadExecutor();
            IPFS ipfs = IPFS.getInstance(getApplicationContext());
            DOCS docs = DOCS.getInstance(getApplicationContext());

            ipfs.setConnectConsumer(connection -> {
                try {
                    buildNotification();
                    if (Multiaddr.isLocalAddress(
                            connection.getRemoteAddress().getAddress())) {

                        service.execute(() -> {

                            Page page = docs.getHomePage();
                            if (page != null) {
                                try {
                                    Cid cid = page.getCid();
                                    Objects.requireNonNull(cid);

                                    IpnsRecord ipnsRecord =
                                            ipfs.createSelfSignedIpnsRecord(page.getSequence(),
                                                    ipfs.encodeIpnsData(cid));

                                    ipfs.push(connection, ipnsRecord);
                                } catch (Throwable throwable) {
                                    LogUtils.error(TAG, throwable);
                                }
                            }
                        });
                    }

                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            });

            ipfs.setClosedConsumer(connection -> buildNotification());
            ipfs.setReachabilityConsumer(reachability -> {
                REACHABILITY = reachability;
                buildNotification();
            });

            registerNetworkCallback();
            registerService();
            STARTED.set(true);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
            stop(); // destroy daemon service, does not make sense to run
        }
    }

}
