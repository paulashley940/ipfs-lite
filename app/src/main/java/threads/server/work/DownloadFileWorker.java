package threads.server.work;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Icon;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.documentfile.provider.DocumentFile;
import androidx.work.Data;
import androidx.work.ForegroundInfo;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Objects;

import threads.lite.utils.ReaderProgress;
import threads.server.InitApplication;
import threads.server.LogUtils;
import threads.server.MainActivity;
import threads.server.R;
import threads.server.core.Content;


public class DownloadFileWorker extends Worker {

    private static final String TAG = DownloadFileWorker.class.getSimpleName();
    private final NotificationManager notificationManager;


    @SuppressWarnings("WeakerAccess")
    public DownloadFileWorker(
            @NonNull Context context,
            @NonNull WorkerParameters params) {
        super(context, params);
        notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    private static OneTimeWorkRequest getWork(@NonNull Uri uri, @NonNull Uri source,
                                              @NonNull String filename, @NonNull String mimeType,
                                              long size) {
        Data.Builder data = new Data.Builder();
        data.putString(Content.URI, uri.toString());
        data.putString(Content.NAME, filename);
        data.putString(Content.TYPE, mimeType);
        data.putLong(Content.SIZE, size);
        data.putString(Content.FILE, source.toString());

        return new OneTimeWorkRequest.Builder(DownloadFileWorker.class)
                .setInputData(data.build())
                .build();
    }

    public static void download(@NonNull Context context, @NonNull Uri uri, @NonNull Uri source,
                                @NonNull String filename, @NonNull String mimeType, long size) {
        WorkManager.getInstance(context).enqueue(getWork(uri, source, filename, mimeType, size));
    }


    @NonNull
    @Override
    public Result doWork() {

        String dest = getInputData().getString(Content.URI);
        Objects.requireNonNull(dest);
        long start = System.currentTimeMillis();
        LogUtils.info(TAG, " start ... " + dest);

        try {

            Uri uriDest = Uri.parse(dest);
            DocumentFile doc = DocumentFile.fromTreeUri(getApplicationContext(), uriDest);
            Objects.requireNonNull(doc);


            long size = getInputData().getLong(Content.SIZE, 0);
            String name = getInputData().getString(Content.NAME);
            Objects.requireNonNull(name);
            String mimeType = getInputData().getString(Content.TYPE);
            Objects.requireNonNull(mimeType);

            String url = getInputData().getString(Content.FILE);
            Objects.requireNonNull(url);
            Uri uri = Uri.parse(url);

            reportProgress(name, 0);

            HttpURLConnection.setFollowRedirects(false);

            URL urlCon = new URL(uri.toString());
            HttpURLConnection huc = (HttpURLConnection) urlCon.openConnection();

            huc.setReadTimeout(30000);
            huc.connect();

            DocumentFile child = doc.createFile(mimeType, name);
            Objects.requireNonNull(child);
            try (InputStream is = huc.getInputStream()) {
                try (OutputStream os = getApplicationContext().
                        getContentResolver().openOutputStream(child.getUri())) {
                    Objects.requireNonNull(os);
                    threads.lite.IPFS.copy(is, os, new ReaderProgress() {
                        @Override
                        public long getSize() {
                            return size;
                        }

                        @Override
                        public void setProgress(int progress) {
                            reportProgress(name, progress);
                        }

                        @Override
                        public boolean doProgress() {
                            return true;
                        }

                        @Override
                        public boolean isCancelled() {
                            return isStopped();
                        }

                    });
                }
            } catch (Throwable e) {
                child.delete();
                buildFailedNotification(name);
                throw e;
            }
            if (!isStopped()) {
                reportProgress(name, 100);
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }
        return Result.success();

    }


    private void buildFailedNotification(@NonNull String name) {

        Notification.Builder builder = new Notification.Builder(
                getApplicationContext(), InitApplication.STORAGE_CHANNEL_ID);

        builder.setContentTitle(getApplicationContext().getString(R.string.download_failed, name));
        builder.setSmallIcon(R.drawable.download);
        Intent defaultIntent = new Intent(getApplicationContext(), MainActivity.class);

        int requestID = (int) System.currentTimeMillis();
        PendingIntent defaultPendingIntent = PendingIntent.getActivity(
                getApplicationContext(), requestID, defaultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

        builder.setContentIntent(defaultPendingIntent);
        builder.setAutoCancel(true);
        builder.setGroup(InitApplication.STORAGE_GROUP_ID);
        Notification notification = builder.build();


        if (notificationManager != null) {
            notificationManager.notify(TAG.hashCode(), notification);
        }
    }


    private void reportProgress(@NonNull String info, int progress) {

        Notification notification = createNotification(info, progress);

        if (notificationManager != null) {
            notificationManager.notify(getId().hashCode(), notification);
        }

        setForegroundAsync(new ForegroundInfo(getId().hashCode(), notification));
    }


    private Notification createNotification(@NonNull String title, int progress) {

        Notification.Builder builder = new Notification.Builder(getApplicationContext(),
                InitApplication.STORAGE_CHANNEL_ID);


        PendingIntent intent = WorkManager.getInstance(getApplicationContext())
                .createCancelPendingIntent(getId());
        String cancel = getApplicationContext().getString(android.R.string.cancel);

        Intent main = new Intent(getApplicationContext(), MainActivity.class);

        int requestID = (int) System.currentTimeMillis();
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), requestID,
                main, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

        Notification.Action action = new Notification.Action.Builder(
                Icon.createWithResource(getApplicationContext(), R.drawable.pause), cancel,
                intent).build();

        builder.setContentTitle(title)
                .setSubText("" + progress + "%")
                .setContentIntent(pendingIntent)
                .setProgress(100, progress, false)
                .setOnlyAlertOnce(true)
                .setSmallIcon(R.drawable.download)
                .addAction(action)
                .setGroup(InitApplication.STORAGE_GROUP_ID)
                .setCategory(Notification.CATEGORY_PROGRESS)
                .setUsesChronometer(true)
                .setOngoing(true);

        return builder.build();
    }

}
