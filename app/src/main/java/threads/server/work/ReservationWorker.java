package threads.server.work;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Constraints;
import androidx.work.ExistingWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import threads.lite.IPFS;
import threads.lite.cid.Multiaddr;
import threads.lite.core.AutonatResult;
import threads.lite.core.Reservation;
import threads.server.LogUtils;
import threads.server.core.DOCS;

public class ReservationWorker extends Worker {
    private static final String TAG = ReservationWorker.class.getSimpleName();


    @SuppressWarnings("WeakerAccess")
    public ReservationWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }


    private static OneTimeWorkRequest getWork(long minutes) {
        Constraints constraints = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build();

        return new OneTimeWorkRequest.Builder(ReservationWorker.class)
                .addTag(TAG)
                .setInitialDelay(minutes, TimeUnit.MINUTES)
                .setConstraints(constraints)
                .build();
    }

    public static void reservations(@NonNull Context context, ExistingWorkPolicy policy, long minutes) {
        WorkManager.getInstance(context).enqueueUniqueWork(
                TAG, policy, getWork(minutes));
    }


    @NonNull
    @Override
    public Result doWork() {

        long start = System.currentTimeMillis();

        LogUtils.error(TAG, "Worker Start " + getId() + " ...");

        try {
            DOCS docs = DOCS.getInstance(getApplicationContext());
            IPFS ipfs = IPFS.getInstance(getApplicationContext());
            ipfs.updateNetwork();
            AutonatResult result = ipfs.autonat();

            // autonat successful, now we should be reachable
            // and we had at least one connection to the server
            // and it is set to  Reachability.GLOBAL

            LogUtils.error(TAG, "Success Autonat : " + result);


            if (!result.success()) {

                try {
                    Set<Reservation> reservations = ipfs.reservations(
                            docs.getSession(), ipfs.getBootstrap(),
                            30);
                    for (Reservation reservation : reservations) {
                        LogUtils.error(TAG, reservation.toString());
                    }

                    long minutes = ipfs.nextReservationCycle();
                    if (minutes == 0) {
                        minutes = 15;
                    }

                    ReservationWorker.reservations(getApplicationContext(),
                            ExistingWorkPolicy.APPEND, minutes);

                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }

            for (Multiaddr ma : ipfs.getIdentity().getMultiaddrs()) {
                LogUtils.error(TAG, ma.toString());
            }

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            LogUtils.error(TAG, "Worker Finish " + getId() +
                    " onStart [" + (System.currentTimeMillis() - start) + "]...");
        }
        return Result.success();
    }
}
