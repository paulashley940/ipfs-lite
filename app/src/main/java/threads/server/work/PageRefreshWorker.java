package threads.server.work;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Constraints;
import androidx.work.ExistingWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import threads.lite.IPFS;
import threads.lite.core.Session;
import threads.server.LogUtils;
import threads.server.core.DOCS;

public class PageRefreshWorker extends Worker {
    private static final String TAG = PageRefreshWorker.class.getSimpleName();


    @SuppressWarnings("WeakerAccess")
    public PageRefreshWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }


    private static OneTimeWorkRequest getWork() {
        Constraints constraints = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build();

        return new OneTimeWorkRequest.Builder(PageRefreshWorker.class)
                .setConstraints(constraints)
                .addTag(TAG)
                .build();
    }

    public static void publish(@NonNull Context context) {
        WorkManager.getInstance(context).enqueueUniqueWork(
                TAG, ExistingWorkPolicy.REPLACE, getWork());
    }


    @NonNull
    @Override
    public Result doWork() {

        long start = System.currentTimeMillis();

        LogUtils.info(TAG, "Worker Start " + getId() + " ...");

        try {
            IPFS ipfs = IPFS.getInstance(getApplicationContext());
            DOCS docs = DOCS.getInstance(getApplicationContext());
            try (Session session = ipfs.createSession(false)) {
                docs.publishPage(session, this::isStopped);
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            LogUtils.info(TAG, "Worker Finish " + getId() +
                    " onStart [" + (System.currentTimeMillis() - start) + "]...");
        }
        return Result.success();
    }


}

