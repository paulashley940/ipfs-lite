package net.luminis.quic;


import java.nio.ByteBuffer;

public interface RawStreamData {

    QuicStream getStream();

    ByteBuffer getStreamData();

    // if the stream data is final
    boolean isFinal();

    // if the stream is terminated by the other side
    boolean isTerminated();

}
