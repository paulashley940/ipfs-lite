package threads.lite.crypto;


import androidx.annotation.NonNull;

import crypto.pb.Crypto;

public abstract class PubKey implements Key {

    private final Crypto.KeyType keyType;

    public PubKey(Crypto.KeyType keyType) {
        super();
        this.keyType = keyType;
    }

    public abstract void verify(byte[] data, byte[] signature) throws Exception;


    @NonNull
    public Crypto.KeyType getKeyType() {
        return this.keyType;
    }
}

