package threads.lite.utils;


import android.os.Build;

import androidx.annotation.NonNull;

import com.google.protobuf.MessageLite;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DataHandler {


    public static int compareUnsigned(byte[] a, byte[] b) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            return Arrays.compareUnsigned(a, b);
        } else {
            return compareUnsignedFallback(a, b);
        }
    }


    private static int compareUnsignedFallback(byte[] a, byte[] b) {
        int minLength = Math.min(a.length, b.length);
        for (int i = 0; i + 7 < minLength; i += 8) {
            long la = Byte.toUnsignedLong(a[i]) << 56 |
                    Byte.toUnsignedLong(a[i + 1]) << 48 |
                    Byte.toUnsignedLong(a[i + 2]) << 40 |
                    Byte.toUnsignedLong(a[i + 3]) << 32 |
                    Byte.toUnsignedLong(a[i + 4]) << 24 |
                    Byte.toUnsignedLong(a[i + 5]) << 16 |
                    Byte.toUnsignedLong(a[i + 6]) << 8 |
                    Byte.toUnsignedLong(a[i + 7]);
            long lb = Byte.toUnsignedLong(b[i]) << 56 |
                    Byte.toUnsignedLong(b[i + 1]) << 48 |
                    Byte.toUnsignedLong(b[i + 2]) << 40 |
                    Byte.toUnsignedLong(b[i + 3]) << 32 |
                    Byte.toUnsignedLong(b[i + 4]) << 24 |
                    Byte.toUnsignedLong(b[i + 5]) << 16 |
                    Byte.toUnsignedLong(b[i + 6]) << 8 |
                    Byte.toUnsignedLong(b[i + 7]);

            if (la != lb)
                return Long.compareUnsigned(la, lb);

        }


        for (int i = 0; i < minLength; i++) {
            int ia = Byte.toUnsignedInt(a[i]);
            int ib = Byte.toUnsignedInt(b[i]);
            if (ia != ib)
                return Integer.compare(ia, ib);
        }

        return a.length - b.length;
    }


    /**
     * Returns the values from each provided array combined into a single array. For example, {@code
     * concat(new int[] {a, b}, new int[] {}, new int[] {c}} returns the array {@code {a, b, c}}.
     *
     * @param arrays zero or more {@code int} arrays
     * @return a single array containing all the values from the source arrays, in order
     */
    public static int[] concat(int[]... arrays) {
        int length = 0;
        for (int[] array : arrays) {
            length += array.length;
        }
        int[] result = new int[length];
        int pos = 0;
        for (int[] array : arrays) {
            System.arraycopy(array, 0, result, pos, array.length);
            pos += array.length;
        }
        return result;
    }

    /**
     * Returns the values from each provided array combined into a single array. For example, {@code
     * concat(new byte[] {a, b}, new byte[] {}, new byte[] {c}} returns the array {@code {a, b, c}}.
     *
     * @param arrays zero or more {@code byte} arrays
     * @return a single array containing all the values from the source arrays, in order
     */
    public static byte[] concat(byte[]... arrays) {
        int length = 0;
        for (byte[] array : arrays) {
            length += array.length;
        }
        byte[] result = new byte[length];
        int pos = 0;
        for (byte[] array : arrays) {
            System.arraycopy(array, 0, result, pos, array.length);
            pos += array.length;
        }
        return result;
    }

    /**
     * Returns the start position of the first occurrence of the specified {@code target} within
     * {@code array}, or {@code -1} if there is no such occurrence.
     *
     * <p>More formally, returns the lowest index {@code i} such that {@code Arrays.copyOfRange(array,
     * i, i + target.length)} contains exactly the same elements as {@code target}.
     *
     * @param array  the array to search for the sequence {@code target}
     * @param target the array to search for as a sub-sequence of {@code array}
     */
    public static int indexOf(byte[] array, byte[] target) {
        if (target.length == 0) {
            return 0;
        }
        outer:
        for (int i = 0; i < array.length - target.length + 1; i++) {
            for (int j = 0; j < target.length; j++) {
                if (array[i + j] != target[j]) {
                    continue outer;
                }
            }
            return i;
        }
        return -1;
    }

    /**
     * Reads a big-endian unsigned short integer from the buffer and advances position.
     */
    public static int getBigEndianUnsignedShort(ByteBuffer buffer) {
        return (buffer.get() << 8 | buffer.get() & 0xFF);
    }

    public static int unsignedVariantSize(int value) {
        int remaining = value >> 7;
        int count = 0;
        while (remaining != 0) {
            remaining >>= 7;
            count++;
        }
        return count + 1;
    }

    public static void writeUnsignedVariant(ByteBuffer out, int value) {
        int remaining = value >>> 7;
        while (remaining != 0) {
            out.put((byte) ((value & 0x7f) | 0x80));
            value = remaining;
            remaining >>>= 7;
        }
        out.put((byte) (value & 0x7f));
    }

    public static int readUnsignedVariant(ByteBuffer in) throws IOException {
        int result = 0;
        int cur;
        int count = 0;
        do {
            cur = in.get() & 0xff;
            result |= (cur & 0x7f) << (count * 7);
            count++;
        } while (((cur & 0x80) == 0x80) && count < 5);
        if ((cur & 0x80) == 0x80) {
            throw new IOException("invalid unsigned variant sequence");
        }
        return result;
    }

    public static boolean isProtocol(byte[] data) {
        if (data.length > 2) {
            if (data[0] == '/' && data[data.length - 1] == '\n') {
                return true;
            } else if (data[0] == 'n' && data[1] == 'a' && data[2] == '\n') {
                return true;
            } else return data[0] == 'l' && data[1] == 's' && data[2] == '\n';
        }
        return false;
    }

    public static byte[] encode(@NonNull MessageLite message) {
        try (ByteArrayOutputStream buf = new ByteArrayOutputStream()) {
            message.writeDelimitedTo(buf);
            return buf.toByteArray();
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }

    public static byte[] encode(byte[] data) {
        int dataLength = DataHandler.unsignedVariantSize(data.length);
        ByteBuffer buffer = ByteBuffer.allocate(dataLength + data.length);
        DataHandler.writeUnsignedVariant(buffer, data.length);
        buffer.put(data);
        return buffer.array();
    }


    public static byte[] encodeProtocols(String... protocols) {
        try (ByteArrayOutputStream buf = new ByteArrayOutputStream()) {
            for (String token : protocols) {
                byte[] data = token.getBytes(StandardCharsets.UTF_8);
                int length = data.length + 1; // 1 is "\n"
                int dataLength = DataHandler.unsignedVariantSize(length);
                ByteBuffer buffer = ByteBuffer.allocate(dataLength);
                DataHandler.writeUnsignedVariant(buffer, length);
                buf.write(buffer.array());
                buf.write(data);
                buf.write('\n');
            }
            return buf.toByteArray();
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }

    @NonNull
    public static List<ByteBuffer> decode(ByteBuffer bytes) throws Exception {
        List<ByteBuffer> frames = new ArrayList<>();
        int lengthBytes = DataHandler.readUnsignedVariant(bytes);
        ByteBuffer frame = ByteBuffer.allocate(lengthBytes);

        if (frame.capacity() <= 0) {
            throw new Exception("invalid length of <= 0");
        } else {
            int length = frame.capacity();
            int read = Math.min(length, bytes.remaining());
            for (int i = 0; i < read; i++) {
                frame.put(bytes.get());
            }
            if (read == frame.capacity()) {
                frames.add(frame);
            }
            // check for a next iteration
            if (bytes.remaining() > 0) {
                frames.addAll(decode(bytes));
            }
        }
        return frames;
    }

}


