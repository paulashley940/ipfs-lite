package threads.lite.utils;

import androidx.annotation.NonNull;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.function.Supplier;

import bitswap.pb.MessageOuterClass;
import threads.lite.cid.Block;
import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Peer;
import threads.lite.cid.PeerId;
import threads.lite.core.BlockStore;
import threads.lite.core.Cancellable;
import threads.lite.core.Connection;
import threads.lite.core.IpnsEntity;
import threads.lite.core.ProtocolHandler;
import threads.lite.core.Session;
import threads.lite.core.StreamHandler;


public final class DummySession implements Session {
    @NonNull
    private final AtomicBoolean closed = new AtomicBoolean(false);

    public DummySession() {
    }

    @Override
    @NonNull
    public Block getBlock(@NonNull Cancellable cancellable, @NonNull Cid cid)
            throws Exception {
        throw new Exception("should not be invoked");
    }

    @Override
    public void receiveMessage(@NonNull Connection connection,
                               @NonNull MessageOuterClass.Message bsm) throws Exception {
        throw new Exception("should not be invoked");
    }


    @Override
    public boolean isClosed() {
        return closed.get();
    }


    @NonNull
    @Override
    public BlockStore getBlockStore() {
        throw new RuntimeException("should not be invoked");
    }

    @Override
    public boolean isFindProvidersActive() {
        return false;
    }

    @NonNull
    @Override
    public Map<String, ProtocolHandler> getProtocols() {
        throw new RuntimeException("should not be invoked");
    }

    @Override
    public void addProtocolHandler(@NonNull ProtocolHandler protocolHandler) throws Exception {
        throw new Exception("should not be invoked");
    }

    @NonNull
    @Override
    public StreamHandler getStreamHandler() {
        return new DummyResponder();
    }

    @Override
    @NonNull
    public Supplier<Set<Connection>> getSwarm() {
        return Collections::emptySet;
    }

    @Override
    public void putValue(@NonNull Cancellable cancellable, @NonNull Consumer<Multiaddr> consumer,
                         @NonNull byte[] key, @NonNull byte[] data) {
        throw new RuntimeException("should not be invoked");
    }

    @Override

    public void findPeer(@NonNull Cancellable cancellable, @NonNull Consumer<Multiaddr> consumer,
                         @NonNull PeerId peerId) {
        throw new RuntimeException("should not be invoked");
    }

    @Override
    public void searchValue(@NonNull Cancellable cancellable,
                            @NonNull Consumer<IpnsEntity> consumer, @NonNull byte[] key) {
        throw new RuntimeException("should not be invoked");
    }

    @Override
    public void findProviders(@NonNull Cancellable cancellable,
                              @NonNull Consumer<Multiaddr> consumer, @NonNull Cid cid) {
        throw new RuntimeException("should not be invoked");
    }

    @Override
    public void provide(@NonNull Cancellable cancellable,
                        @NonNull Consumer<Multiaddr> consumer, @NonNull Cid cid) {
        throw new RuntimeException("should not be invoked");
    }

    @Override
    public void findClosestPeers(@NonNull Cancellable cancellable,
                                 @NonNull Consumer<Multiaddr> consumer, @NonNull PeerId peerId) {
        throw new RuntimeException("should not be invoked");
    }

    @NonNull
    @Override
    public Set<Peer> getRoutingPeers() {
        return Collections.emptySet();
    }

    @Override
    public void close() {
        closed.set(true);
    }
}

