package threads.lite.dht;

import androidx.annotation.NonNull;

import java.math.BigInteger;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

import threads.lite.cid.ID;
import threads.lite.cid.Peer;


public final class QueryPeer implements Comparable<QueryPeer> {

    @NonNull
    private final Peer peer;
    @NonNull
    private final BigInteger distance;
    @NonNull
    private final AtomicReference<PeerState> state = new AtomicReference<>(PeerState.PeerHeard);

    private QueryPeer(@NonNull Peer peer, @NonNull BigInteger distance) {
        this.peer = peer;
        this.distance = distance;
    }

    public static QueryPeer create(@NonNull Peer peer, @NonNull ID key) {
        BigInteger distance = Util.distance(key, peer.getID());
        return new QueryPeer(peer, distance);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QueryPeer queryPeer = (QueryPeer) o;
        return Objects.equals(distance, queryPeer.distance);
    }

    @Override
    public int hashCode() {
        return distance.hashCode();
    }

    @NonNull
    public PeerState getState() {
        return state.get();
    }

    public void setState(@NonNull PeerState state) {
        this.state.set(state);
    }

    @NonNull
    @Override
    public String toString() {
        return "QueryPeer{" +
                "id=" + peer.getPeerId().toBase58() +
                ", distance=" + distance +
                ", state=" + state +
                '}';
    }

    @Override
    public int compareTo(QueryPeer o) {
        return distance.compareTo(o.distance);
    }

    @NonNull
    public Peer getPeer() {
        return peer;
    }


}
