package threads.lite.dht;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentSkipListSet;

import threads.lite.IPFS;


public class QueryPeerSet extends ConcurrentSkipListSet<QueryPeer> {

    public QueryPeerSet() {
        super(QueryPeer::compareTo);
    }

    // TryAdd adds the peer p to the peer set.
    // If the peer is already present, no action is taken.
    // Otherwise, the peer is added with state set to PeerHeard.
    public boolean tryAdd(@NonNull QueryPeer peer) {

        if (size() > IPFS.DHT_ALPHA) {
            QueryPeer last = getLast();
            if (last != null) {
                if (last.compareTo(peer) <= 0) {
                    return false;
                }
            }
        }

        return this.add(peer);
    }

    @Nullable
    private QueryPeer getLast() {
        Iterator<QueryPeer> iterator = descendingIterator();
        while (iterator.hasNext()) {
            QueryPeer last = iterator.next();
            PeerState state = last.getState();
            if (state == PeerState.PeerWaiting || state == PeerState.PeerHeard) {
                return last;
            }
        }
        return null;
    }

    // GetClosestNInStates returns the closest to the key peers, which are in one of the given states.
    // It returns n peers or less, if fewer peers meet the condition.
    // The returned peers are sorted in ascending order by their distance to the key.
    private List<QueryPeer> getClosestNInStates(int maxLength, @NonNull List<PeerState> states) {
        List<QueryPeer> peers = new ArrayList<>();
        int count = 0;
        for (QueryPeer queryPeer : this) {
            if (states.contains(queryPeer.getState())) {
                peers.add(queryPeer);
                count++;
                if (count == maxLength) {
                    break;
                }
            }
        }
        return peers;
    }

    @NonNull
    private List<QueryPeer> getClosestInStates(int maxLength, @NonNull List<PeerState> states) {
        return getClosestNInStates(maxLength, states);
    }

    public boolean isUnreachable() {
        for (QueryPeer queryPeer : this) {
            if (queryPeer.getState() != PeerState.PeerUnreachable) {
                return false;
            }
        }
        return true;
    }

    public List<QueryPeer> nextHeardPeers(int maxLength) {
        // The peers we query next should be ones that we have only Heard about.
        return getClosestInStates(maxLength, Collections.singletonList(PeerState.PeerHeard));

    }

}
