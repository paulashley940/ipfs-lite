package threads.lite.noise;

import androidx.annotation.NonNull;

import com.southernstorm.noise.protocol.CipherState;

import net.luminis.quic.QuicStream;

import threads.lite.core.Stream;
import threads.lite.core.Transport;

public class SecuredTransport implements Transport {
    @NonNull
    private final CipherState sender;
    @NonNull
    private final CipherState receiver;
    @NonNull
    private final QuicStream stream;

    public SecuredTransport(@NonNull QuicStream stream, @NonNull CipherState sender,
                            @NonNull CipherState receiver) {
        this.stream = stream;
        this.sender = sender;
        this.receiver = receiver;
    }

    @NonNull
    public CipherState getSender() {
        return sender;
    }

    @NonNull
    public CipherState getReceiver() {
        return receiver;
    }

    @Override
    public Type getType() {
        return Type.SECURED;
    }

    @Override
    public Stream getStream() {
        return SecuredStream.createStream(this);
    }

    @NonNull
    public QuicStream getQuicStream() {
        return stream;
    }
}
