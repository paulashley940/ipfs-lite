package threads.lite.bitswap;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

import threads.lite.IPFS;
import threads.lite.cid.Cid;
import threads.lite.cid.PeerId;
import threads.lite.core.Connection;

public class BitSwapRegistry extends ConcurrentHashMap<Cid, Timer> {

    private final ConcurrentHashMap<Cid, Integer> delays = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<Cid, List<PeerId>> sends = new ConcurrentHashMap<>();

    private BitSwapRegistry() {
        super();
    }

    public static BitSwapRegistry getInstance() {
        return new BitSwapRegistry();
    }

    public void register(@NonNull Cid cid) {
        this.put(cid, new Timer());
        this.delays.put(cid, 0);
        this.sends.put(cid, new ArrayList<>());
    }

    public void unregister(@NonNull Cid cid) {
        Timer timer = this.remove(cid);
        if (timer != null) {
            timer.cancel();
        }
        this.delays.remove(cid);
        this.sends.remove(cid);
    }

    public boolean isRegistered(@NonNull Cid cid) {
        return this.containsKey(cid);
    }


    private boolean hasSend(@NonNull Cid cid, @NonNull PeerId peerId) {
        List<PeerId> peerIds = sends.get(cid);
        Objects.requireNonNull(peerIds);
        if (peerIds.contains(peerId)) {
            return true;
        } else {
            peerIds.add(peerId);
            return false;
        }
    }

    public void scheduleWants(@NonNull Connection connection, @NonNull Cid cid,
                              @NonNull TimerTask timerTask) {

        Timer timer = this.get(cid);
        if (timer != null) {

            if (hasSend(cid, connection.remotePeerId())) {
                return;
            }

            int delay = delays.computeIfAbsent(cid, cid1 -> 0);

            timer.schedule(timerTask, delay);

            delays.put(cid, delay + (IPFS.BITSWAP_REQUEST_DELAY * 1000));
        }

    }
}
