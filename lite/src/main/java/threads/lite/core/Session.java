package threads.lite.core;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.Closeable;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;


public interface Session extends Closeable, Routing, BitSwap {

    // returns all supported protocols within the session
    @NonNull
    Map<String, ProtocolHandler> getProtocols();

    @NonNull
    default Set<String> getProtocolNames() {
        return new HashSet<>(getProtocols().keySet());
    }

    @Nullable
    default ProtocolHandler getProtocolHandler(@NonNull String protocol) {
        return getProtocols().get(protocol);
    }

    // add a protocol handler to the supported list of protocols.
    // Note: a protocol handler is only invoked, when a remote peer initiate a
    // stream over an existing connection
    void addProtocolHandler(@NonNull ProtocolHandler protocolHandler) throws Exception;

    // the stream handler which is invoked on peer initiated streams
    @NonNull
    StreamHandler getStreamHandler();

    // returns the block store where all data is stored
    @NonNull
    BlockStore getBlockStore();

    // returns true, when in bitswap the provider search is enabled
    boolean isFindProvidersActive();

    // returns all connections of the swarm
    @NonNull
    Supplier<Set<Connection>> getSwarm();

    // returns true when session is closed, otherwise false (note: when session is closed
    // operations on a session will fail [Interrupt Exception]
    boolean isClosed();
}

