package threads.lite.host;

import androidx.annotation.NonNull;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.function.Supplier;

import bitswap.pb.MessageOuterClass;
import threads.lite.LogUtils;
import threads.lite.bitswap.BitSwapHandler;
import threads.lite.bitswap.BitSwapManager;
import threads.lite.cid.Block;
import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Peer;
import threads.lite.cid.PeerId;
import threads.lite.core.BitSwap;
import threads.lite.core.BlockStore;
import threads.lite.core.Cancellable;
import threads.lite.core.Connection;
import threads.lite.core.IpnsEntity;
import threads.lite.core.ProtocolHandler;
import threads.lite.core.Session;
import threads.lite.core.StreamHandler;
import threads.lite.dht.KadDht;
import threads.lite.ident.IdentityHandler;
import threads.lite.ipns.IpnsService;
import threads.lite.utils.MultistreamHandler;


public final class LiteSession implements Session {
    private static final String TAG = LiteSession.class.getSimpleName();
    @NonNull
    private final BitSwap bitSwap;
    @NonNull
    private final BlockStore blockStore;
    @NonNull
    private final Map<String, ProtocolHandler> protocols = new ConcurrentHashMap<>();
    @NonNull
    private final KadDht routing;
    @NonNull
    private final AtomicBoolean closed = new AtomicBoolean(false);
    @NonNull
    private final Supplier<Set<Connection>> swarm;
    private final boolean findProvidersActive;

    public LiteSession(@NonNull BlockStore blockStore, @NonNull LiteHost host,
                       @NonNull Supplier<Set<Connection>> swarm,
                       boolean findProvidersActive) {
        this.blockStore = blockStore;
        this.swarm = swarm;
        this.findProvidersActive = findProvidersActive;
        this.bitSwap = new BitSwapManager(host, this);
        this.routing = new KadDht(host, host.getPeerStore(), new IpnsService());

        // add the default
        try {
            addProtocolHandler(new MultistreamHandler());
            addProtocolHandler(new LitePushHandler(host));
            addProtocolHandler(new LitePullHandler(host));
            addProtocolHandler(new IdentityHandler(host, this));
            addProtocolHandler(new BitSwapHandler(this));
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }


    public void addProtocolHandler(@NonNull ProtocolHandler protocolHandler) throws Exception {

        if (isClosed()) throw new IllegalStateException("Session is closed");

        // some small tests
        String protocol = protocolHandler.getProtocol();
        Objects.requireNonNull(protocol);

        if (protocol.isEmpty()) {
            throw new Exception("invalid protocol name");
        }

        if (!protocol.startsWith("/")) {
            throw new Exception("invalid protocol name");
        }

        if (protocols.containsKey(protocol)) {
            throw new Exception("protocol name already exists");
        }

        protocols.put(protocol, protocolHandler);


    }

    @NonNull
    public Map<String, ProtocolHandler> getProtocols() {
        // important, protocols should note be modified "outside"
        return Collections.unmodifiableMap(protocols);
    }

    @Override
    @NonNull
    public Block getBlock(@NonNull Cancellable cancellable, @NonNull Cid cid)
            throws Exception {
        return bitSwap.getBlock(cancellable, cid);
    }

    @Override
    public void receiveMessage(@NonNull Connection connection,
                               @NonNull MessageOuterClass.Message bsm) throws Exception {
        bitSwap.receiveMessage(connection, bsm);
    }

    @NonNull
    @Override
    public BlockStore getBlockStore() {
        return blockStore;
    }

    @Override
    public boolean isFindProvidersActive() {
        return findProvidersActive;
    }

    @NonNull
    @Override
    public StreamHandler getStreamHandler() {
        return new LiteResponder(protocols);
    }

    @Override
    @NonNull
    public Supplier<Set<Connection>> getSwarm() {
        return swarm;
    }

    @Override
    public void putValue(@NonNull Cancellable cancellable, @NonNull Consumer<Multiaddr> consumer,
                         @NonNull byte[] key, @NonNull byte[] data) {
        routing.putValue(cancellable, consumer, key, data);
    }

    @Override
    public void findPeer(@NonNull Cancellable cancellable, @NonNull Consumer<Multiaddr> consumer,
                         @NonNull PeerId peerId) {
        routing.findPeer(cancellable, consumer, peerId);
    }

    @Override

    public void searchValue(@NonNull Cancellable cancellable,
                            @NonNull Consumer<IpnsEntity> consumer, @NonNull byte[] key) {
        routing.searchValue(cancellable, consumer, key);
    }

    @Override
    public void findProviders(@NonNull Cancellable cancellable,
                              @NonNull Consumer<Multiaddr> consumer, @NonNull Cid cid) {
        routing.findProviders(cancellable, consumer, cid);
    }

    @Override
    public void provide(@NonNull Cancellable cancellable,
                        @NonNull Consumer<Multiaddr> consumer, @NonNull Cid cid) {
        routing.provide(cancellable, consumer, cid);
    }

    @Override
    public void findClosestPeers(@NonNull Cancellable cancellable,
                                 @NonNull Consumer<Multiaddr> consumer, @NonNull PeerId peerId) {
        routing.findClosestPeers(cancellable, consumer, peerId);
    }

    @NonNull
    @Override
    public Set<Peer> getRoutingPeers() {
        return routing.getRoutingPeers();
    }

    @Override
    public void close() {
        try {
            bitSwap.close();
            routing.close();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            closed.set(true);
        }
    }

    @Override
    public boolean isClosed() {
        return closed.get();
    }
}

