package threads.lite.minidns;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.function.Supplier;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.cid.ProtocolSupport;
import threads.lite.cid.Type;
import threads.lite.minidns.record.Data;
import threads.lite.minidns.record.Record;
import threads.lite.minidns.record.TXT;


public final class DnsResolver {
    public static final String DNS_ADDR = "dnsaddr=";
    public static final String DNS_LINK = "dnslink=";
    public static final Set<Inet4Address> STATIC_IPV4_DNS_SERVERS = new CopyOnWriteArraySet<>();
    public static final Set<Inet6Address> STATIC_IPV6_DNS_SERVERS = new CopyOnWriteArraySet<>();
    private static final String IPv4 = "/" + Type.IP4 + "/";
    private static final String IPv6 = "/" + Type.IP6 + "/";
    private static final String DNS_ADDR_PATH = "/" + Type.DNSADDR + "/";
    private static final String DNS_PATH = "/" + Type.DNS + "/";
    private static final String DNS4_PATH = "/" + Type.DNS4 + "/";
    private static final String DNS6_PATH = "/" + Type.DNS6 + "/";
    private static final String TAG = DnsResolver.class.getSimpleName();

    static {
        try {
            Inet4Address googleV4Dns = DnsUtility.ipv4From(IPFS.MINIDNS_DNS_SERVER_IP4);
            STATIC_IPV4_DNS_SERVERS.add(googleV4Dns);
        } catch (IllegalArgumentException e) {
            LogUtils.error(TAG, "Could not add static IPv4 DNS Server " + e.getMessage());
        }

        try {
            Inet6Address googleV6Dns = DnsUtility.ipv6From(IPFS.MINIDNS_DNS_SERVER_IP6);
            STATIC_IPV6_DNS_SERVERS.add(googleV6Dns);
        } catch (IllegalArgumentException e) {
            LogUtils.error(TAG, "Could not add static IPv6 DNS Server " + e.getMessage());
        }
    }

    @NonNull
    public static String resolveDnsLink(@NonNull DnsClient client, @NonNull String host) {

        Set<String> txtRecords = retrieveTxtRecords(client, "_dnslink.".concat(host));
        for (String txtRecord : txtRecords) {
            try {
                if (txtRecord.startsWith(DNS_LINK)) {
                    return txtRecord.replaceFirst(DNS_LINK, "");
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, host + " " + throwable.getClass().getName());
            }
        }
        return "";
    }

    @NonNull
    private static Set<String> retrieveTxtRecords(@NonNull DnsClient client,
                                                  @NonNull String host) {
        Set<String> txtRecords = new HashSet<>();
        try {
            DnsQueryResult result = client.query(host, Record.TYPE.TXT);
            DnsMessage response = result.response;
            List<Record<? extends Data>> records = response.answerSection;
            for (Record<? extends Data> record : records) {
                Data payload = record.getPayload();
                if (payload instanceof TXT) {
                    TXT text = (TXT) payload;
                    txtRecords.add(text.getText());
                } else {
                    LogUtils.warning(TAG, payload.toString());
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, host + " " + throwable.getClass().getName());
        }
        return txtRecords;
    }

    @Nullable
    public static Multiaddr resolveDns(@NonNull ProtocolSupport protocolSupport,
                                       @NonNull String multiaddress) {
        if (!multiaddress.startsWith(DNS_PATH)) {
            return null;
        }
        String query = multiaddress.replaceFirst(DNS_PATH, "");
        return createMultiaddr(query, protocolSupport);
    }

    @Nullable
    public static Multiaddr resolveDns4Address(@NonNull ProtocolSupport protocolSupport,
                                               @NonNull String multiaddress) {
        if (!multiaddress.startsWith(DNS4_PATH)) {
            return null;
        }
        String query = multiaddress.replaceFirst(DNS4_PATH, "");
        return createMultiaddr(query, protocolSupport);
    }


    @Nullable
    public static Multiaddr resolveDns6Address(@NonNull ProtocolSupport protocolSupport,
                                               @NonNull String multiaddress) {
        if (!multiaddress.startsWith(DNS6_PATH)) {
            return null;
        }
        String query = multiaddress.replaceFirst(DNS6_PATH, "");
        return createMultiaddr(query, protocolSupport);
    }

    @Nullable
    private static Multiaddr createMultiaddr(@NonNull String multiaddress,
                                             @NonNull ProtocolSupport protocolSupport) {

        try {
            String host = multiaddress.split("/")[0];
            InetAddress address = InetAddress.getByName(host);
            String ip = IPv4;
            if (address instanceof Inet6Address) {
                ip = IPv6;
            }
            String hostAddress = address.getHostAddress();
            Objects.requireNonNull(hostAddress);
            String addr = ip.concat(multiaddress.replaceFirst(host, hostAddress));
            Multiaddr multiaddr = Multiaddr.create(addr);

            if (multiaddr.protocolSupported(protocolSupport, true)) {
                return multiaddr;
            }
        } catch (Throwable ignore) {
            LogUtils.debug(TAG, multiaddress + " not supported");
        }
        return null;
    }


    @Nullable
    public static Multiaddr resolveDns(@NonNull ProtocolSupport protocolSupport,
                                       @NonNull Multiaddr multiaddr) {
        if (multiaddr.isDns6()) {
            return resolveDns6Address(protocolSupport, multiaddr.toString());
        }
        if (multiaddr.isDns4()) {
            return resolveDns4Address(protocolSupport, multiaddr.toString());
        }
        if (multiaddr.isDns()) {
            return resolveDns(protocolSupport, multiaddr.toString());
        }
        throw new IllegalStateException("not a dns multiaddress");
    }

    @NonNull
    public static List<Multiaddr> resolveDnsaddr(@NonNull DnsClient dnsClient,
                                                 @NonNull ProtocolSupport protocolSupport,
                                                 @NonNull Multiaddr multiaddr) {
        List<Multiaddr> multiaddrs = new ArrayList<>();
        if (!multiaddr.isDnsaddr()) {
            return multiaddrs;
        }
        String host = multiaddr.getHost();
        Objects.requireNonNull(host);

        try {
            PeerId peerId = multiaddr.getPeerId();
            Set<Multiaddr> addresses = resolveDnsaddrHost(dnsClient, protocolSupport, host);
            for (Multiaddr addr : addresses) {
                PeerId cmpPeerId = addr.getPeerId();
                if (Objects.equals(cmpPeerId, peerId)) {
                    multiaddrs.add(addr);
                }
            }
        } catch (Throwable ignore) {
        }
        return multiaddrs;
    }


    @NonNull
    public static Set<Multiaddr> resolveDnsaddrHost(@NonNull DnsClient dnsClient,
                                                    @NonNull ProtocolSupport protocolSupport,
                                                    @NonNull String host) {
        return resolveDnsAddressInternal(dnsClient, protocolSupport, host, new HashSet<>());
    }

    @NonNull
    public static Set<Multiaddr> resolveDnsAddressInternal(@NonNull DnsClient dnsClient,
                                                           @NonNull ProtocolSupport protocolSupport,
                                                           @NonNull String host,
                                                           @NonNull Set<String> hosts) {
        Set<Multiaddr> multiAddresses = new HashSet<>();
        // recursion protection
        if (hosts.contains(host)) {
            return multiAddresses;
        }
        hosts.add(host);

        Set<String> txtRecords = retrieveTxtRecords(dnsClient, "_dnsaddr." + host);

        for (String txtRecord : txtRecords) {
            try {
                if (txtRecord.startsWith(DNS_ADDR)) {
                    String testRecordReduced = txtRecord.replaceFirst(DNS_ADDR, "");
                    if (testRecordReduced.startsWith(DNS_ADDR_PATH)) {
                        String query = testRecordReduced.replaceFirst(DNS_ADDR_PATH, "");
                        String child = query.split("/")[0];
                        multiAddresses.addAll(resolveDnsAddressInternal(dnsClient, protocolSupport,
                                child, hosts));
                    } else if (testRecordReduced.startsWith(DNS4_PATH)) {
                        Multiaddr multiaddr = resolveDns4Address(protocolSupport, testRecordReduced);
                        if (multiaddr != null) {
                            multiAddresses.add(multiaddr);
                        }
                    } else if (testRecordReduced.startsWith(DNS6_PATH)) {
                        Multiaddr multiaddr = resolveDns6Address(protocolSupport, testRecordReduced);
                        if (multiaddr != null) {
                            multiAddresses.add(multiaddr);
                        }
                    } else if (testRecordReduced.startsWith(DNS_PATH)) {
                        Multiaddr multiaddr = resolveDns(protocolSupport, testRecordReduced);
                        if (multiaddr != null) {
                            multiAddresses.add(multiaddr);
                        }
                    } else {
                        Multiaddr multiaddr = Multiaddr.create(testRecordReduced);
                        if (multiaddr.protocolSupported(protocolSupport, true)) {
                            multiAddresses.add(multiaddr);
                        }

                    }
                }
            } catch (Throwable throwable) {
                LogUtils.debug(TAG, throwable.getMessage());
            }
        }
        return multiAddresses;
    }

    @NonNull
    public static DnsClient getInstance(@NonNull Supplier<List<InetAddress>> settingSupplier) {
        return new DnsClient(settingSupplier, new DnsCache(128));
    }

}
